﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorldWebServer.DataAccess
{
    public class WorldDbContextFactory
    {
        public WorldDBContext Create(string connectionString)
        {
            var optionsBuilder = new DbContextOptionsBuilder<WorldDBContext>();
            optionsBuilder.UseMySQL(connectionString);
            var dbContext = new WorldDBContext(optionsBuilder.Options);
            return dbContext;
        }
    }
}
