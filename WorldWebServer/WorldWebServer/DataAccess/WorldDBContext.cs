﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorldWebServer.Models;

namespace WorldWebServer.DataAccess
{
    public class WorldDBContext :DbContext
    {
        public WorldDBContext(DbContextOptions<WorldDBContext> options)
            :base(options)
        {

        }

        public DbSet<Country> Country { get; set; }
        public DbSet<City> City { get; set; }
    }

     
}
