﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorldWebServer.DataAccess;

namespace WorldWebServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Countries")]
    public class CountriesController : Controller
    {
        private WorldDBContext dbContext;

        public CountriesController()
        {
            var connectionString = "server=localhost;port=3306;database=world;userid=root;pwd=12345;sslmode=none";
            dbContext = new WorldDbContextFactory().Create(connectionString);
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(this.dbContext.Country.ToList());
        }
    }
}