﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorldWebServer.DataAccess;
using WorldWebServer.Models;

namespace WorldWebServer.Controllers
{
    [Produces("application/json")]
    [Route("api/Cities")]
    public class CitiesController : Controller
    {
        private WorldDBContext dbContext;

        public CitiesController()
        {
            var connectionString = "server=localhost;port=3306;database=world;userid=root;pwd=12345;sslmode=none";
            dbContext = new WorldDbContextFactory().Create(connectionString);
        }

        [HttpGet]
        public ActionResult Get()
        {
            return Ok(dbContext.City.ToList());
        }

        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            var city = dbContext.City.Where(x => x.ID == id).SingleOrDefault();
            if (city != null)
                return Ok(city);
            else
                return NotFound();
        }

        [HttpGet("cc/{cc}")]
        public ActionResult Get(string cc)
        {
            var cities = dbContext.City.Where(x => string.Equals(x.CountryCode, cc, StringComparison.CurrentCultureIgnoreCase)).ToList();
            if (cities != null)
                return Ok(cities);
            else
                return NotFound();
        }

        [HttpPost]
        public ActionResult Post([FromBody] City city)
        {
            if (ModelState.IsValid)
            {
                dbContext.City.Add(city);
                dbContext.SaveChanges();
                return Created($"api/cities/{city.ID}", city);
            }
            else
                return BadRequest();
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id,[FromBody] City city)
        {
            var target = dbContext.City.Where(x => x.ID == id).SingleOrDefault();
            if (target != null)
            {
                target.Name = city.Name;
                target.CountryCode = city.CountryCode;
                target.District = city.District;
                target.Population = city.Population;
                dbContext.SaveChanges();
                return Ok();
            }
            else
                return NotFound();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var city = dbContext.City.Where(x => x.ID == id).SingleOrDefault();
            if (city != null)
            {
                dbContext.City.Remove(city);
                dbContext.SaveChanges();
                return Ok();
            }
            else
                return NotFound();
        }
    }
}